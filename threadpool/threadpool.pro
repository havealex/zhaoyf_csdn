#-------------------------------------------------
#
# Project created by QtCreator 2018-01-23T14:46:18
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = threadpool
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    condition.cpp \
    threadpool.cpp

HEADERS += \
    condition.h \
    threadpool.h
